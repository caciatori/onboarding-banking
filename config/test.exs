use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :bank_account_onboarding, BankAccountOnboarding.Repo,
  username: "luizcaciatori",
  password: "",
  database: "bank_account_onboarding_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :bank_account_onboarding, BankAccountOnboardingWeb.Endpoint,
  http: [port: 4002],
  server: false

config :bank_account_onboarding, BankAccountOnboarding.Users.Guardian,
  secret_key: "geCirgiNRVtmIfrZuJFnFlB9aAWzv3P14cLDID3Uyc/qIQWA8t0wb1nGgapu0ho9"

# Print only warnings and errors during test
config :logger, level: :warn
