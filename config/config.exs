# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :bank_account_onboarding,
  ecto_repos: [BankAccountOnboarding.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :bank_account_onboarding, BankAccountOnboardingWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "vkalpQCIKgOdfdh0raoBR4PpKSm7CS8fXIR9q7SSqPfRlvGfEXkhv+PdsFs3J29d",
  render_errors: [view: BankAccountOnboardingWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: BankAccountOnboarding.PubSub,
  live_view: [signing_salt: "RPjw7OLs"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :bank_account_onboarding, BankAccountOnboarding.Users.Guardian,
  issuer: "bank_account_onboarding"

config :bank_account_onboarding, BankAccountOnboarding.Encrypt.Vault,
  ciphers: [
    default: {
      Cloak.Ciphers.AES.GCM,
      tag: "AES.GCM.V1", key: Base.decode64!("73LTapieWxFcdBgNPksRL8bi3yxk4tirWGi2I1nwHdc=")
    }
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
