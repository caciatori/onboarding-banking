defmodule BankAccountOnboarding.UsersTest do
  use BankAccountOnboarding.DataCase

  alias Argon2
  alias BankAccountOnboarding.Users

  describe "users" do
    alias BankAccountOnboarding.Users.User

    @valid_attrs %{email: "user@email.com", password: "some password"}
    @invalid_attrs %{email: nil, password: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Users.create_user()

      user
    end

    test "get_user!/1 returns the user with given id" do
      fixture = user_fixture()
      user = Users.get_user!(fixture.id)
      assert user.id == fixture.id
      assert user.email == fixture.email
    end

    test "get_user/1 returns the user with given id" do
      fixture = user_fixture()
      user = Users.get_user(fixture.id)
      assert user.id == fixture.id
      assert user.email == fixture.email
    end

    test "get_user/1 returns nil when id doesn't exist" do
      refute Users.get_user(Ecto.UUID.autogenerate())
    end

    test "get_user_by_email/1 returns the user with given id" do
      fixture = user_fixture()
      user = Users.get_user_by_email(fixture.email)
      assert user.id == fixture.id
      assert user.email == fixture.email
    end

    test "get_user_by_email/1 returns nil when id doesn't exist" do
      refute Users.get_user_by_email("invalid@email.com")
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Users.create_user(@valid_attrs)
      assert {:ok, user} = Argon2.check_pass(user, "some password", hash_key: :encrypted_password)
      assert user.email == "user@email.com"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Users.create_user(@invalid_attrs)
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Users.change_user(user)
    end
  end
end
