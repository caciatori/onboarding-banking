defmodule BankAccountOnboarding.Accounts.AccountTest do
  use BankAccountOnboarding.DataCase

  alias BankAccountOnboarding.Accounts
  alias BankAccountOnboarding.Accounts.Account
  alias BankAccountOnboarding.Users

  @valid_attrs %{
    "birth_date" => "2010-04-17",
    "city" => "some city",
    "country" => "some country",
    "cpf" => "22564776060",
    "email" => "some@email.com",
    "gender" => "some gender",
    "name" => "some name",
    "state" => "some state"
  }

  def account_fixture(attrs \\ %{}) do
    {:ok, user} =
      Users.create_user(%{"email" => @valid_attrs["email"], "password" => "my-strong-pass"})

    {:ok, account} =
      attrs
      |> Enum.into(@valid_attrs)
      |> Map.put("user_id", user.id)
      |> Accounts.upsert_account()

    account
  end

  describe "completed/1" do
    test "with a account full filled changes the status to completed" do
      account = account_fixture(%{"status" => "pending", "referral_code" => nil})
      assert %{status: status} = Account.completed(account)
      assert status == "completed"
    end

    test "with a account partial filled doesn't changes the status" do
      account = account_fixture(%{"status" => "pending", "name" => nil})
      assert %{status: status} = Account.completed(account)
      assert status == "pending"
    end
  end

  describe "generate_referral_code/1" do
    test "with a completed account returns a valid code" do
      account = account_fixture() |> Map.put("status", "completed")
      assert %{referral_code: referral_code} = Account.generate_referral_code(account)
      refute is_nil(referral_code)
      refute referral_code == ""
    end

    test "with a pending account doesn't returns a code" do
      assert %{referral_code: referral_code} =
               Account.generate_referral_code(%Account{status: "pending"})

      assert is_nil(referral_code)
    end
  end
end
