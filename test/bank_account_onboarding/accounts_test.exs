defmodule BankAccountOnboarding.AccountsTest do
  use BankAccountOnboarding.DataCase

  alias BankAccountOnboarding.{Accounts, Users}
  alias BankAccountOnboarding.Repo

  describe "accounts" do
    alias BankAccountOnboarding.Accounts.{Account, Indication}

    @valid_attrs %{
      "birth_date" => "2010-04-17",
      "city" => "Campinas",
      "country" => "Brasil",
      "cpf" => "95146126046",
      "email" => "valentina@email.com",
      "gender" => "Feminino",
      "name" => "Valentina dos Santos",
      "referral_code" => nil,
      "state" => "São Paulo"
    }
    @update_attrs %{
      "birth_date" => "2011-05-18",
      "city" => "Brasília",
      "country" => "Brasil",
      "cpf" => "95146126046",
      "email" => "claudia@email.com",
      "gender" => "Feminino",
      "name" => "Claudia Fernandes",
      "state" => "Distrito Federal"
    }
    @invalid_attrs %{
      "birth_date" => nil,
      "city" => nil,
      "country" => nil,
      "cpf" => nil,
      "email" => nil,
      "gender" => nil,
      "name" => nil,
      "referral_code" => nil,
      "state" => nil,
      "status" => nil
    }

    def account_fixture(attrs \\ %{}) do
      {:ok, user} =
        Users.create_user(%{"email" => @valid_attrs["email"], "password" => "my-strong-pass"})

      {:ok, account} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Map.put("user_id", user.id)
        |> Accounts.upsert_account()

      account
    end

    # def account_fixture(attrs \\ %{}) do
    #   {:ok, account} =
    #     attrs
    #     |> Enum.into(@valid_attrs)
    #     |> Accounts.upsert_account()

    #   account
    # end

    test "get_account!/1 returns the account with given id" do
      fixture = account_fixture()
      assert %Account{} = account = Accounts.get_account!(fixture.id)
      assert account.birth_date == fixture.birth_date
      assert account.city == fixture.city
      assert account.country == fixture.country
      assert account.cpf == fixture.cpf
      assert account.email == fixture.email
      assert account.gender == fixture.gender
      assert account.name == fixture.name
      assert account.referral_code == fixture.referral_code
      assert account.state == fixture.state
      assert account.status == fixture.status
    end

    test "get_account_by_user_id/1 returns the account with given a valid id" do
      fixture = account_fixture()
      assert %Account{} = account = Accounts.get_account_by_user_id(fixture.user_id)
      assert account.birth_date == fixture.birth_date
      assert account.city == fixture.city
      assert account.country == fixture.country
      assert account.cpf == fixture.cpf
      assert account.email == fixture.email
      assert account.gender == fixture.gender
      assert account.name == fixture.name
      assert account.referral_code == fixture.referral_code
      assert account.state == fixture.state
      assert account.status == fixture.status
    end

    test "change_account/1 returns a account changeset" do
      account = account_fixture()
      assert %Ecto.Changeset{} = Accounts.change_account(account)
    end

    test "upsert_account/1 with valid data and not existing account creates one" do
      {:ok, user} =
        Users.create_user(%{"email" => @valid_attrs["email"], "password" => "my-strong-pass"})

      valid_attrs = Map.put(@valid_attrs, "user_id", user.id)

      assert {:ok, %Account{} = account} = Accounts.upsert_account(valid_attrs)
      assert account.birth_date == "2010-04-17"
      assert account.city == "Campinas"
      assert account.country == "Brasil"
      assert account.cpf == "95146126046"
      assert account.email == "valentina@email.com"
      assert account.gender == "Feminino"
      assert account.name == "Valentina dos Santos"
      assert account.state == "São Paulo"
      assert account.status == "completed"
      assert not is_nil(account.referral_code)
    end

    test "upsert_account/1 with valid data and already exists one updates the account" do
      fixture = account_fixture()
      assert {:ok, %Account{} = account} = Accounts.upsert_account(@update_attrs)
      assert account.cpf == fixture.cpf
      assert account.id == fixture.id
      assert account.birth_date == "2011-05-18"
      assert account.city == "Brasília"
      assert account.country == "Brasil"
      assert account.email == "claudia@email.com"
      assert account.gender == "Feminino"
      assert account.name == "Claudia Fernandes"
      assert account.state == "Distrito Federal"
      assert account.status == "completed"
      assert not is_nil(account.referral_code)
    end

    test "upsert_account/1 with invalid data when tries to create a new one returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.upsert_account(@invalid_attrs)
    end

    test "upsert_account/1 with referral_code of a completed account on payload creates an account and indication" do
      fixture = account_fixture()

      {:ok, user} =
        Users.create_user(%{"email" => "gilberto@email.com", "password" => "my-strong-pass"})

      attrs = %{
        "birth_date" => "2010-04-17",
        "city" => "São Paulo",
        "country" => "Brasil",
        "cpf" => "22564776060",
        "email" => user.email,
        "gender" => "Masculino",
        "name" => "Gilberto Pereira",
        "referral_code" => fixture.referral_code,
        "state" => "São Paulo",
        "user_id" => user.id
      }

      assert {:ok, %Account{} = account} = Accounts.upsert_account(attrs)

      indications =
        fixture.id
        |> Accounts.get_account!()
        |> Accounts.list_indications()

      %Indication{} = fixture_indication = List.first(indications)
      assert fixture_indication.name == account.name
      assert fixture_indication.account_indicated_id == account.id
      assert fixture_indication.account_id == fixture.id
    end

    test "upsert_account/1 with referral_code of a pending account on payload creates just an account" do
      {:ok, user_that_indicated} =
        Users.create_user(%{"email" => "paula@email.com", "password" => "my-strong-pass"})

      {:ok, fixture} =
        Accounts.upsert_account(%{
          "cpf" => "95146126046",
          "referral_code" => "12345678",
          "user_id" => user_that_indicated.id
        })

      {:ok, user} =
        Users.create_user(%{"email" => "gilberto@email.com", "password" => "my-strong-pass"})

      attrs = %{
        "birth_date" => "2010-04-17",
        "city" => "São Paulo",
        "country" => "Brasil",
        "cpf" => "22564776060",
        "email" => user.email,
        "gender" => "Masculino",
        "name" => "Gilberto Pereira",
        "referral_code" => fixture.referral_code,
        "state" => "São Paulo",
        "user_id" => user.id
      }

      assert {:ok, %Account{} = account} = Accounts.upsert_account(attrs)

      %Account{indications: indications} =
        fixture.id
        |> Accounts.get_account!()
        |> Repo.preload([:indications])

      assert indications == []
    end

    test "list_indications/1 returns an empty array when account is pending" do
      {:ok, user_that_indicated} =
        Users.create_user(%{"email" => "paula@email.com", "password" => "my-strong-pass"})

      {:ok, account} =
        Accounts.upsert_account(%{
          "cpf" => "95146126046",
          "referral_code" => "12345678",
          "user_id" => user_that_indicated.id
        })

      assert Accounts.list_indications(account) == []
    end

    test "list_indications/1 return a indication list when account is completed" do
      fixture = account_fixture()

      {:ok, user} =
        Users.create_user(%{"email" => "gilberto@email.com", "password" => "my-strong-pass"})

      attrs = %{
        "birth_date" => "2010-04-17",
        "city" => "São Paulo",
        "country" => "Brasil",
        "cpf" => "22564776060",
        "email" => user.email,
        "gender" => "Masculino",
        "name" => "Gilberto Pereira",
        "referral_code" => fixture.referral_code,
        "state" => "São Paulo",
        "user_id" => user.id
      }

      assert {:ok, %Account{} = account} = Accounts.upsert_account(attrs)

      indications =
        fixture.id
        |> Accounts.get_account!()
        |> Accounts.list_indications()

      assert length(indications) == 1
    end
  end
end
