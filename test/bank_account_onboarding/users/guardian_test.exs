defmodule BankAccountOnboarding.Users.GuardianTest do
  use BankAccountOnboarding.DataCase

  alias BankAccountOnboarding.Users
  alias BankAccountOnboarding.Users.{Guardian, User}

  @valid_attrs %{email: "user@email.com", password: "some password"}

  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(@valid_attrs)
      |> Users.create_user()

    user
  end

  describe "subject_for_token/2" do
    test "given a valid user returns a tuple with its id" do
      user_id = "03127b7f-f65c-43ed-a4ff-8df10d33430d"
      assert {:ok, ^user_id} = Guardian.subject_for_token(%User{id: user_id}, nil)
    end

    test "given an invalid user returns an error tuple" do
      assert {:error, :invalid_user} = Guardian.subject_for_token("invalid_param", nil)
    end
  end

  describe "resource_from_claims/1" do
    test "given a valid user_id returns a user" do
      fixture = user_fixture()

      assert {:ok, user} = Guardian.resource_from_claims(%{"sub" => fixture.id})
      assert user.id == fixture.id
      assert user.email == fixture.email
    end

    test "given a valid user_id returns an error" do
      assert {:error, :user_not_found} =
               Guardian.resource_from_claims(%{"sub" => Ecto.UUID.autogenerate()})
    end
  end

  describe "autheticate_user/1" do
    test "with valid data returns a user" do
      fixture = user_fixture()

      assert {:ok, user, _token} = Guardian.autheticate_user(fixture.email, "some password")
      assert {:ok, user} = Argon2.check_pass(user, "some password", hash_key: :encrypted_password)
    end

    test "with invalid data returns an error" do
      fixture = user_fixture()

      assert {:error, :unauthorized} =
               Guardian.autheticate_user(fixture.email, "some wrong password")
    end

    test "when not exits a user returns an error" do
      assert {:error, :unauthorized} =
               Guardian.autheticate_user("wrong@email.com", "some wrong password")
    end
  end
end
