defmodule BankAccountOnboardingWeb.AccountControllerTest do
  use BankAccountOnboardingWeb.ConnCase

  alias BankAccountOnboarding.{Accounts, Users}
  alias BankAccountOnboarding.Accounts.{Account, Indication}
  alias BankAccountOnboarding.Repo
  alias BankAccountOnboarding.Users.Guardian

  @create_attrs %{
    "birth_date" => "2010-04-17",
    "city" => "Belo Horizonte",
    "country" => "Brasil",
    "cpf" => "22564776060",
    "email" => "joao@email.com",
    "gender" => "Masculino",
    "name" => "João Marcos de Souza",
    "referral_code" => nil,
    "state" => "Minas Gerais"
  }
  @update_attrs %{
    "birth_date" => "2011-05-18",
    "city" => "Curitiba",
    "country" => "Brasil",
    "cpf" => "22564776060",
    "email" => "marcela@email.com",
    "gender" => "Feminino",
    "name" => "Marcela de Moura",
    "referral_code" => nil,
    "state" => "Paraná"
  }
  @invalid_attrs %{
    "birth_date" => nil,
    "city" => nil,
    "country" => nil,
    "cpf" => nil,
    "email" => nil,
    "gender" => nil,
    "name" => nil,
    "referral_code" => nil,
    "state" => nil
  }

  def fixture(:account, attrs \\ %{}) do
    {:ok, user} =
      Users.create_user(%{"email" => @create_attrs["email"], "password" => "my-strong-pass"})

    attrs =
      @create_attrs
      |> Map.put("user_id", user.id)
      |> Map.merge(attrs)

    {:ok, account} = Accounts.upsert_account(attrs)
    account
  end

  setup %{conn: conn} do
    {:ok, user} =
      Users.create_user(%{
        email: "some@email.com",
        password: "some_password"
      })

    {:ok, _user, jwt_token} = Guardian.autheticate_user(user.email, user.password)

    conn =
      conn
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer #{jwt_token}")

    {:ok, conn: conn}
  end

  describe "create account" do
    test "with a full filled payload creates a account with status completed and referral code",
         %{conn: conn} do
      conn = post(conn, Routes.account_path(conn, :create), account: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      account = Accounts.get_account!(id)
      assert account.birth_date == @create_attrs["birth_date"]
      assert account.city == @create_attrs["city"]
      assert account.country == @create_attrs["country"]
      assert account.cpf == @create_attrs["cpf"]
      assert account.email == @create_attrs["email"]
      assert account.gender == @create_attrs["gender"]
      assert account.name == @create_attrs["name"]
      assert account.state == @create_attrs["state"]
      assert account.status == "completed"
      assert not is_nil(account.referral_code)
    end

    test "with just a cpf on payload creates a account with status pending without referral code",
         %{conn: conn} do
      conn = post(conn, Routes.account_path(conn, :create), account: %{cpf: @create_attrs["cpf"]})
      assert %{"id" => id} = json_response(conn, 201)["data"]

      account = Accounts.get_account!(id)
      assert account.cpf == @create_attrs["cpf"]
      assert account.status == "pending"
      refute account.referral_code
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.account_path(conn, :create), account: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update account" do
    test "with a valid payload update name and email in account", %{conn: conn} do
      %{id: id} = fixture = fixture(:account, Map.put(@invalid_attrs, "cpf", "22564776060"))

      payload = %{
        "email" => "gabriel@email.com",
        "cpf" => fixture.cpf,
        "name" => "Gabriel da Silva",
        "birth_date" => "2000-01-01"
      }

      conn = post(conn, Routes.account_path(conn, :create), account: payload)
      assert %{"id" => ^id} = json_response(conn, 201)["data"]

      account = Accounts.get_account!(id)

      assert account.cpf == payload["cpf"]
      assert account.name == payload["name"]
      assert account.birth_date == payload["birth_date"]
      assert account.email == payload["email"]
      assert account.status == "pending"
      refute account.referral_code
    end

    test "with a valid payload update all attributes on account and set as completed", %{
      conn: conn
    } do
      %{id: id} = fixture(:account, Map.put(@invalid_attrs, "cpf", "22564776060"))

      conn = post(conn, Routes.account_path(conn, :create), account: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 201)["data"]

      account = Accounts.get_account!(id)

      assert account.birth_date == @update_attrs["birth_date"]
      assert account.city == @update_attrs["city"]
      assert account.country == @update_attrs["country"]
      assert account.cpf == @update_attrs["cpf"]
      assert account.email == @update_attrs["email"]
      assert account.gender == @update_attrs["gender"]
      assert account.name == @update_attrs["name"]
      assert account.state == @update_attrs["state"]
      assert account.status == "completed"
      assert not is_nil(account.referral_code)
    end
  end

  describe "create an indication account" do
    test "with a valid payload create an indication account", %{conn: conn} do
      fixture = fixture(:account)

      payload = %{
        "cpf" => "11773486080",
        "name" => "João Pereira",
        "email" => "joao@email.com",
        "birth_date" => "2000-03-08",
        "city" => "Curitiba",
        "country" => "Brasil",
        "gender" => "Masculino",
        "state" => "Paraná",
        "referral_code" => fixture.referral_code
      }

      conn = post(conn, Routes.account_path(conn, :create), account: payload)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      %Account{indications: indications} =
        fixture.id
        |> Accounts.get_account!()
        |> Repo.preload([:indications])

      %Indication{} = fixture_indication = List.first(indications)
      assert fixture_indication.account_indicated_id == id
      assert fixture_indication.account_id == fixture.id
    end
  end

  describe "list indications" do
    test "render indications from an account", %{conn: conn} do
      fixture = fixture(:account)

      payload = %{
        "cpf" => "11773486080",
        "name" => "Gilberto Pereira",
        "email" => "gilberto@email.com",
        "birth_date" => "2000-03-08",
        "city" => "Curitiba",
        "country" => "Brasil",
        "gender" => "Masculino",
        "state" => "Paraná",
        "referral_code" => fixture.referral_code
      }

      conn = post(conn, Routes.account_path(conn, :create), account: payload)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      {:ok, _user, jwt_token} = Guardian.autheticate_user(fixture.email, "my-strong-pass")

      new_conn =
        build_conn()
        |> put_req_header("authorization", "Bearer #{jwt_token}")

      new_conn = get(new_conn, Routes.account_path(new_conn, :list_indications))
      assert indications = json_response(new_conn, 200)["data"]

      assert [
               %{
                 "account_indicated_id" => ^id,
                 "name" => "Gilberto Pereira"
               }
             ] = indications
    end

    test "render error when account isn't completed", %{conn: _conn} do
      {:ok, user} =
        Users.create_user(%{"email" => @create_attrs["email"], "password" => "my-strong-pass"})

      {:ok, _account} = Accounts.upsert_account(%{"user_id" => user.id, "cpf" => "22564776060"})
      {:ok, _user, jwt_token} = Guardian.autheticate_user(user.email, "my-strong-pass")

      new_conn =
        build_conn()
        |> put_req_header("authorization", "Bearer #{jwt_token}")

      new_conn = get(new_conn, Routes.account_path(new_conn, :list_indications))
      assert json_response(new_conn, 406)
    end
  end
end
