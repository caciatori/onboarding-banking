defmodule BankAccountOnboardingWeb.UserControllerTest do
  use BankAccountOnboardingWeb.ConnCase

  alias BankAccountOnboarding.Users

  @create_attrs %{
    email: "some@email.com",
    password: "some password"
  }

  @invalid_attrs %{email: nil, password: nil}

  def fixture(:user) do
    {:ok, user} = Users.create_user(@create_attrs)
    user
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "user signup" do
    test "renders user when data is valid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :sign_up), user: @create_attrs)
      assert %{"email" => "some@email.com", "token" => _token} = json_response(conn, 201)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :sign_up), user: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "user signin" do
    test "renders user when data is valid", %{conn: conn} do
      fixture = fixture(:user)
      payload = %{"email" => fixture.email, "password" => fixture.password}
      conn = post(conn, Routes.user_path(conn, :sign_in), payload)
      assert %{"email" => "some@email.com", "token" => _token} = json_response(conn, 201)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      payload = %{"email" => "invalid@email.com", "password" => "password"}
      conn = post(conn, Routes.user_path(conn, :sign_in), payload)
      assert json_response(conn, 401)
    end
  end
end
