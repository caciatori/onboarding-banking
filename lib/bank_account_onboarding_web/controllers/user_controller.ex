defmodule BankAccountOnboardingWeb.UserController do
  use BankAccountOnboardingWeb, :controller

  alias BankAccountOnboarding.Users
  alias BankAccountOnboarding.Users.{Guardian, User}

  action_fallback BankAccountOnboardingWeb.FallbackController

  def sign_up(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Users.create_user(user_params),
         {:ok, token, _claims} <- Guardian.encode_and_sign(user) do
      conn
      |> put_status(:created)
      |> render("user.json", %{user: user, token: token})
    end
  end

  def sign_in(conn, %{"email" => email, "password" => password}) do
    with {:ok, user, token} <- Guardian.autheticate_user(email, password) do
      conn
      |> put_status(:created)
      |> render("user.json", %{user: user, token: token})
    end
  end
end
