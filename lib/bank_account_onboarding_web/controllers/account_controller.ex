defmodule BankAccountOnboardingWeb.AccountController do
  use BankAccountOnboardingWeb, :controller

  alias BankAccountOnboarding.Accounts
  alias BankAccountOnboarding.Accounts.Account
  alias BankAccountOnboarding.Users.{Guardian, User}

  action_fallback BankAccountOnboardingWeb.FallbackController

  def create(conn, %{"account" => account_params}) do
    %User{id: user_id} = Guardian.Plug.current_resource(conn)
    account_params = Map.put(account_params, "user_id", user_id)

    with {:ok, %Account{} = account} <- Accounts.upsert_account(account_params) do
      conn
      |> put_status(:created)
      |> render("show.json", account: account)
    end
  end

  def list_indications(conn, _params) do
    with %User{id: user_id} <- Guardian.Plug.current_resource(conn),
         %Account{status: "completed"} = account <- Accounts.get_account_by_user_id(user_id) do
      indications = Accounts.list_indications(account)

      conn
      |> put_status(:ok)
      |> render("indications.json", indications: indications)
    else
      %Account{status: "pending"} ->
        {:error, :not_acceptable}

      nil ->
        {:error, :not_found}

      {:error, reason} ->
        {:error, reason}
    end
  end
end
