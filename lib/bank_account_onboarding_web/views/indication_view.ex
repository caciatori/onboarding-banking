defmodule BankAccountOnboardingWeb.IndicationView do
  use BankAccountOnboardingWeb, :view

  def render("indication.json", %{indication: indication}) do
    %{
      name: indication.name,
      account_indicated_id: indication.account_indicated_id
    }
  end
end
