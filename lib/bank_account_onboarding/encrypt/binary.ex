defmodule BankAccountOnboarding.Encrypt.Binary do
  @moduledoc """
  This module defines the ecto custom type for binary
  """
  use Cloak.Ecto.Binary, vault: BankAccountOnboarding.Encrypt.Vault
end
