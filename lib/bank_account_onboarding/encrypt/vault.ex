defmodule BankAccountOnboarding.Encrypt.Vault do
  @moduledoc """
  This module implements Cloak.Vault behaviour to be used on the custom binary type
  """
  use Cloak.Vault, otp_app: :bank_account_onboarding
end
