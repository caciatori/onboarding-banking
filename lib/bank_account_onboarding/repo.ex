defmodule BankAccountOnboarding.Repo do
  use Ecto.Repo,
    otp_app: :bank_account_onboarding,
    adapter: Ecto.Adapters.Postgres
end
