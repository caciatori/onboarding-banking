defmodule BankAccountOnboarding.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias BankAccountOnboarding.Repo
  alias BankAccountOnboarding.Accounts.{Account, Indication}

  @doc """
  Gets a single account.

  Raises `Ecto.NoResultsError` if the Account does not exist.

  ## Examples

      iex> get_account!(123)
      %Account{}

      iex> get_account!(456)
      ** (Ecto.NoResultsError)

  """
  def get_account!(id), do: Repo.get!(Account, id)

  @doc """
  Return an account that belongs to a user.

  Returns nil if not exists.

  ## Examples

      iex> get_account_by_user_id("123")
      %Account{}

      iex> get_account_by_user_id("456")
      nil

  """

  def get_account_by_user_id(user_id), do: Repo.get_by(Account, user_id: user_id)

  @doc """
  List all indications for completed accounts.

  Returns an empty list if account is pending.

  ## Examples

      iex> list_indications(%Account{status: "completed"})
      [%Indication{}]

      iex> list_indications(%Account{status: "pending"})
      []

  """
  def list_indications(%Account{status: "pending"}), do: []

  def list_indications(%Account{status: "completed"} = account) do
    %{indications: indications} =
      account
      |> Repo.preload([:indications])

    indications
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking account changes.

  ## Examples

      iex> change_account(account)
      %Ecto.Changeset{data: %Account{}}

  """
  def change_account(%Account{} = account, attrs \\ %{}) do
    Account.changeset(account, attrs)
  end

  @doc """
  Create or update a account.

  ## Examples

      iex> upsert_account(account, %{field: new_value})
      {:ok, %Account{}}

      iex> upsert_account(account, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def upsert_account(%{"cpf" => nil} = attrs) do
    {:error, Account.changeset(%Account{}, attrs)}
  end

  def upsert_account(attrs) do
    case create_or_update_account(attrs) do
      {:ok, account} ->
        values =
          account
          |> Account.completed()
          |> Account.generate_referral_code()
          |> Map.take([:status, :referral_code])

        if has_referral_code?(attrs) do
          create_indication(account, Map.get(attrs, "referral_code"))
        end

        update_account_status(account, values)

      {:error, reason} ->
        {:error, reason}
    end
  end

  defp has_referral_code?(attrs) do
    Map.get(attrs, "referral_code") != nil and Map.get(attrs, "referral_code") != ""
  end

  defp create_or_update_account(%{"cpf" => cpf} = attrs) do
    case Repo.get_by(Account, cpf_hash: cpf) do
      nil ->
        create_account(attrs)

      %Account{} = account ->
        update_account(account, attrs)
    end
  end

  defp create_account(attrs \\ %{}) do
    %Account{}
    |> Account.changeset(attrs)
    |> Repo.insert()
  end

  defp update_account(%Account{} = account, attrs) do
    account
    |> Account.changeset(attrs)
    |> Repo.update()
  end

  defp update_account_status(%Account{} = account, attrs) do
    account
    |> Account.internal_changeset(attrs)
    |> Repo.update()
  end

  defp create_indication(%Account{id: account_indicated_id, name: name}, referral_code) do
    with %Account{id: id, status: "completed"} <-
           Repo.get_by(Account, referral_code: referral_code) do
      attrs = %{
        name: name,
        account_indicated_id: account_indicated_id,
        account_id: id
      }

      %Indication{}
      |> Indication.changeset(attrs)
      |> Repo.insert()
    end
  end
end
