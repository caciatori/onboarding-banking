defmodule BankAccountOnboarding.Users.Guardian do
  @moduledoc """
  The module that implements the functions for enconde and decode JWT tokens
  """
  use Guardian, otp_app: :bank_account_onboarding

  alias BankAccountOnboarding.Users
  alias BankAccountOnboarding.Users.User

  @doc """
  Returns a tuple with the user_id

  ## Examples

      iex> subject_for_token(%User{id: "03127b7f-f65c-43ed-a4ff-8df10d33430d"}, nil)
      {:ok, "03127b7f-f65c-43ed-a4ff-8df10d33430d"}

      iex> subject_for_token(%{}, nil)
      {:error, :invalid_user}
  """
  @spec subject_for_token(User.t(), any) :: {:ok, String.t()} | {:error, :invalid_user}
  def subject_for_token(%User{id: user_id}, _claim), do: {:ok, user_id}
  def subject_for_token(_user, _claim), do: {:error, :invalid_user}

  @doc """
  Returns a tuple with a user

  ## Examples

      iex> resource_from_claims(%{"sub" => "some_user_id"})
      {:ok, %User{}}

      iex> resource_from_claims(%{"sub" => "some_invalid_user_id"})
      {:error, :user_not_found}
  """
  @spec resource_from_claims(map) :: {:ok, User.t()} | {:error, :user_not_found}
  def resource_from_claims(%{"sub" => user_id}) do
    case Users.get_user(user_id) do
      nil ->
        {:error, :user_not_found}

      %User{} = user ->
        {:ok, user}
    end
  end

  @doc """
  Authenticate a user

  ## Examples

      iex> autheticate_user("valid@email.com", "password")
      {:ok, %User{}, "jwt-token"}

      iex> autheticate_user("invalid@email.com", "password")
      {:error, :unauthorized}
  """
  @spec autheticate_user(String.t(), String.t()) ::
          {:ok, User.t(), String.t()} | {:error, :unauthorized}
  def autheticate_user(email, password) do
    case Users.get_user_by_email(email) do
      nil ->
        Argon2.no_user_verify()
        {:error, :unauthorized}

      %User{} = user ->
        verify_password(user, password)
    end
  end

  defp verify_password(%User{} = user, password) do
    if Argon2.verify_pass(password, user.encrypted_password) do
      {:ok, token, _claim} = Guardian.encode_and_sign(__MODULE__, user, %{}, ttl: {5, :minute})
      {:ok, user, token}
    else
      {:error, :unauthorized}
    end
  end
end
