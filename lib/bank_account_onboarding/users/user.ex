defmodule BankAccountOnboarding.Users.User do
  @moduledoc """
  The User schema
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias Argon2
  alias BankAccountOnboarding.Accounts.Account
  alias BankAccountOnboarding.Encrypt.Binary, as: BinaryType
  alias Cloak.Ecto.SHA256
  alias Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "users" do
    field :email, BinaryType
    field :email_hash, SHA256
    field :encrypted_password, :string
    field :password, :string, virtual: true

    has_one :account, Account
    timestamps()
  end

  @required_fields ~w(email password)a

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> validate_format(:email, ~r/^[A-Za-z0-9._-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/)
    |> validate_length(:password, min: 8)
    |> unique_constraint(:email_hash)
    |> put_password_hash()
    |> put_hashed_fields()
  end

  defp put_hashed_fields(changeset) do
    changeset
    |> put_change(:email_hash, get_field(changeset, :email))
  end

  defp put_password_hash(%Changeset{valid?: true, changes: %{password: password}} = changeset) do
    change(changeset, encrypted_password: Argon2.hash_pwd_salt(password))
  end

  defp put_password_hash(changeset), do: changeset
end
