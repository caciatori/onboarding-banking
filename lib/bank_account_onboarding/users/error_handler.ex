defmodule BankAccountOnboarding.Users.ErrorHandler do
  @moduledoc """
  The module responsible for implement the Guardian.Plug.ErrorHandler behaviour
  """
  import Plug.Conn

  @behaviour Guardian.Plug.ErrorHandler

  @impl Guardian.Plug.ErrorHandler
  @spec auth_error(Plug.Conn.t(), {atom(), any}, any) :: Plug.Conn.t()
  def auth_error(conn, {type, _reason}, _opts) do
    conn
    |> put_resp_content_type("text/plain")
    |> send_resp(401, to_string(type))
  end
end
