defmodule BankAccountOnboarding.Users.Pipeline do
  @moduledoc """
  The Guardian Pipeline module that verify each request on application
  """
  use Guardian.Plug.Pipeline,
    otp_app: :bank_account_onboarding,
    error_handler: BankAccountOnboarding.Users.ErrorHandler,
    module: BankAccountOnboarding.Users.Guardian

    plug Guardian.Plug.VerifyHeader
    plug Guardian.Plug.LoadResource
    plug Guardian.Plug.EnsureAuthenticated
end
