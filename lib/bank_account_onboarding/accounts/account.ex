defmodule BankAccountOnboarding.Accounts.Account do
  @moduledoc """
  Account schema
  """
  use Ecto.Schema
  import Ecto.Changeset
  import Brcpfcnpj.Changeset

  alias BankAccountOnboarding.Accounts.{Account, Indication}
  alias BankAccountOnboarding.Encrypt.Binary, as: BinaryType
  alias BankAccountOnboarding.Users.User
  alias Cloak.Ecto.SHA256

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "accounts" do
    field :birth_date, BinaryType
    field :city, :string
    field :country, :string
    field :cpf, BinaryType
    field :cpf_hash, SHA256
    field :email, BinaryType
    field :gender, :string
    field :name, BinaryType
    field :referral_code, :string
    field :state, :string
    field :status, :string, default: "pending"

    has_many :indications, Indication

    belongs_to :user, User

    timestamps()
  end

  @required_fields ~w(cpf user_id)a
  @optional_fields ~w(gender city state country)a
  @encripted_fields ~w(name email birth_date)a

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, @required_fields ++ @optional_fields ++ @encripted_fields)
    |> validate_required(@required_fields)
    |> validate_email()
    |> validate_cpf(:cpf)
    |> put_hashed_fields()
  end

  @internal_fields ~w(status referral_code)a
  @allowed_statuses ~w(completed pending)
  @doc false
  def internal_changeset(account, attrs) do
    account
    |> cast(attrs, @internal_fields)
    |> validate_inclusion(:status, @allowed_statuses)
  end

  @email_regex ~r/^[A-Za-z0-9._-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/
  defp validate_email(changeset) do
    validate_change(changeset, :email, fn field, value ->
      has_value? = !is_nil(value) and value != ""

      if has_value? && !Regex.match?(@email_regex, value) do
        [{field, "Invalid Email"}]
      else
        []
      end
    end)
  end

  defp put_hashed_fields(changeset) do
    changeset
    |> put_change(:cpf_hash, get_field(changeset, :cpf))
  end

  @doc """
  Change account status for completed

  ## Examples

      iex> completed(valid_account)
      %Account{status: "completed"}

      iex> completed(invalid_account)
      %Account{status: "pending"}
  """
  def completed(%Account{} = account) do
    case all_fields_are_filled?(account) do
      true ->
        Map.put(account, :status, "completed")

      false ->
        Map.put(account, :status, "pending")
    end
  end

  defp all_fields_are_filled?(%Account{} = account) do
    account
    |> Map.delete(:referral_code)
    |> Map.from_struct()
    |> Enum.all?(fn {_key, value} -> not is_nil(value) end)
  end

  @doc """
  Returns a random code with 8 digits

  ## Examples

      iex> generate_referral_code(completed_account)
      %Account{referral_code: 0000_0000}

      iex> generate_referral_code(incompleted_account)
      %Account{referral_code: nil}
  """
  def generate_referral_code(%Account{status: "completed"} = account) do
    Map.put(account, :referral_code, random_code())
  end

  def generate_referral_code(%Account{} = account), do: account

  defp random_code do
    Enum.random(10_000_000..99_909_999) |> Integer.to_string()
  end
end
