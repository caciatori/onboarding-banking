defmodule BankAccountOnboarding.Accounts.Indication do
  @moduledoc """
  Indication schema
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias BankAccountOnboarding.Accounts.Account
  alias BankAccountOnboarding.Encrypt.Binary, as: BinaryType

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "indications" do
    field :name, BinaryType
    field :account_indicated_id, :binary_id

    belongs_to :account, Account
    timestamps()
  end

  @required_fields ~w(name account_indicated_id account_id)a

  @doc false
  def changeset(indication, attrs) do
    indication
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
  end
end
