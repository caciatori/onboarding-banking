defmodule BankAccountOnboarding.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      BankAccountOnboarding.Repo,
      # Start the Telemetry supervisor
      BankAccountOnboardingWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: BankAccountOnboarding.PubSub},
      # Start the Endpoint (http/https)
      BankAccountOnboardingWeb.Endpoint,
      # Start a worker by calling: BankAccountOnboarding.Worker.start_link(arg)
      # {BankAccountOnboarding.Worker, arg}
      BankAccountOnboarding.Encrypt.Vault
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: BankAccountOnboarding.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    BankAccountOnboardingWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
