defmodule BankAccountOnboarding.Users do
  @moduledoc """
  The Users context.
  """
  alias Argon2
  alias BankAccountOnboarding.Repo
  alias BankAccountOnboarding.Users.User

  @doc """
  Gets a single user.

  Returns nil if the User does not exist.

  ## Examples

      iex> get_user(123)
      %User{}

      iex> get_user(456)
      nil

  """
  def get_user(id), do: Repo.get(User, id)

  @doc """
  Gets a single user.

  Raises Ecto.NoResultsError if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      %Ecto.NoResultsError{}

  """
  def get_user!(id), do: Repo.get(User, id)

  @doc """
  Gets a single user by email.

  Returns nil if the User does not exist.

  ## Examples

      iex> get_user("valid@email.com")
      %User{}

      iex> get_user("invalid@email.com")
      nil

  """
  def get_user_by_email(email), do: Repo.get_by(User, email_hash: email)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end
end
