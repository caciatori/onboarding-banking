# BankAccountOnboarding

Para começar a utilizar o projeto clone o repositório e siga os passos abaixo:
  * Altere os dados de acesso ao banco nos arquivos `dev.exs` e `test.exs`
  * Execute o comando para baixar as dependencias `mix deps.get`
  * Execute `mix ecto.setup` para criar a `base` e executar as `migrations`
  * Execute para inicializar a aplicação `mix phx.server`

A aplicação estará disponível no [`localhost:4000`](http://localhost:4000).

## Solução

A aplicação conta com uma tabela de `accounts` aonde fica os dados do cadastro do usuário e uma de `indications` aonde fica salvo as informações de quem indicou e do indicado. 

Todos os request para criar uma conta ou ver as indicações o usuário deve estar autenticado.

## Utilizando a API

## Autenticação

  * Antes de tudo utilize o [POST - localhost:4000/api/users/signup](localhost:4000/api/users/signup) para criar um usuário
  * Abaixo um exemplo de payload para o signup
  
  ```json
    {
      "user": {
        "email": "joao@email.com",
        "password": "senhasecreta"
      }
    }
  ```
  
  * A resposta do `POST - localhost:4000/api/users/signup` será um token que deve ser utilizado no header das chamadas para a criação da conta.
  
  ```json
    {
      "email": "joao@email.com",
      "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJiYW5rX2FjY291bnRfb25ib2FyZGluZyIsImV4cCI6MTU5NjM5MjM5OSwiaWF0IjoxNTkzOTczMTk5LCJpc3MiOiJiYW5rX2FjY291bnRfb25ib2FyZGluZyIsImp0aSI6IjQyMzNhOTFjLTNmMjYtNDgzYy04YWM4LTUwODg2NmVkMjIzMyIsIm5iZiI6MTU5Mzk3MzE5OCwic3ViIjoiMDFmZThhZjQtZmRjMC00OTM0LTkyZDktMjI5ZGNhODBiNjQ5IiwidHlwIjoiYWNjZXNzIn0.-SiOPQ_a0HIn7B8HyMby3-GGaiMuVX5W3aerdAW38YnPsR8OkwJsgIlvAluMbuZUmAmWzG0uke-qGvey9lQZtQ"
    }
  ```
  
  * Caso o seu token expire você pode fazer o login na aplicação utilizando a seguinte URL [POST - localhost:4000/api/users/signin](localhost:4000/api/users/signin)
  * Payload de exemplo para a chamada de signin
  ```json
    {
      "email": "joao@email.com",
      "password": "senhasecreta"
    }
  ```
  * A resposta do `POST - localhost:4000/api/users/signin` também será um token que deve ser utilizado no header das chamadas para a criação da conta.
  ```json
    {
      "email": "joao@email.com",
      "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJiYW5rX2FjY291bnRfb25ib2FyZGluZyIsImV4cCI6MTU5Mzk3NDQ5MSwiaWF0IjoxNTkzOTc0MTkxLCJpc3MiOiJiYW5rX2FjY291bnRfb25ib2FyZGluZyIsImp0aSI6IjU2ZjI0OWIwLWQzNzItNDQ4Ny04MzQxLWE1Yzc2MGIwOWU4ZiIsIm5iZiI6MTU5Mzk3NDE5MCwic3ViIjoiMmZiZTBlYjgtMzA5ZC00ODliLTk3OTQtN2ZmYzZmOGJkNTk2IiwidHlwIjoiYWNjZXNzIn0.DdBdnLB17PFxelrdvtK5jOY2s7_tp1U7K-68WdW-MSuoLFgJS98SDMPHlWWpn18CFx72ol6dBr9NccM1elaKzg"
    }
  ```
  
## Criando um conta

  * URL `POST localhost:4000/api/accounts`
  * No header do request deve ser colocado `Authorization: Bearer jwt token`
  * Payload sem código de indicação
  ```json
    {
      "account": {
        "cpf": "68669933004",
        "name": "Roberto da Silva",
        "email": "roberto@email.com",
        "birth_date": "1994-03-08",
        "city": "Curitiba",
        "country": "Brasil",
        "gender": "Masculino",
        "state": "Paraná"
      }
    }
  ```
  
  * Payload com código de indicação
  ```json
    {
      "account": {
        "cpf": "68669933004",
        "name": "Roberto da Silva",
        "email": "roberto@email.com",
        "birth_date": "1994-03-08",
        "city": "Curitiba",
        "country": "Brasil",
        "gender": "Masculino",
        "state": "Paraná",
        "referral_code": "12870551"
      }
    }
  ``` 

## Listando as Indicações

   * `URL GET localhost:4000/api/accounts/indications`
   * No header do request deve ser colocado `Authorization: Bearer jwt token`
   * Modelo de resposta caso a conta esteja com o status `completed`
   ```json
   {
      "data": [
        {
          "account_indicated_id": "1723ee26-7205-47a2-8b19-604f656ee2d5",
          "name": "Gustavo da Silva Pereira"
        }
      ]
    }
   ```
   
   * Modelo de reposta caso a conta esteja `pending`
   ```json
    {
      "errors": {
        "detail": "Not Acceptable"
      }
    }
   ```
   
## Contribuindo

Abra um issue e fork o projeto para a sua conta após isso abra um PR para o projeto principal com as suas modificações.

## License

MIT License © Luis Gustavo Caciatori
  
   
   
  