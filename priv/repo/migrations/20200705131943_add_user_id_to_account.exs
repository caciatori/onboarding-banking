defmodule BankAccountOnboarding.Repo.Migrations.AddUserIdToAccount do
  use Ecto.Migration

  def change do
    alter table(:accounts) do
      add :user_id, references(:users, on_delete: :nothing, type: :binary_id)
    end

    create index(:accounts, [:user_id])
  end
end
