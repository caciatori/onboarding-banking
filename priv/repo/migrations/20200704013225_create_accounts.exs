defmodule BankAccountOnboarding.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  alias BankAccountOnboarding.Encrypt.Binary

  def change do
    create table(:accounts, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :binary
      add :email, :binary
      add :cpf, :binary
      add :cpf_hash, :binary
      add :birth_date, :binary
      add :gender, :string
      add :city, :string
      add :state, :string
      add :country, :string
      add :referral_code, :string
      add :status, :string, default: "pending"

      timestamps()
    end
  end
end
