defmodule BankAccountOnboarding.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :email, :binary
      add :email_hash, :binary
      add :encrypted_password, :string

      timestamps()
    end

    create unique_index(:users, [:email_hash])
  end
end
