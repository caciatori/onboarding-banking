defmodule BankAccountOnboarding.Repo.Migrations.CreateIndications do
  use Ecto.Migration

  def change do
    create table(:indications, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :binary
      add :account_indicated_id, references(:accounts, on_delete: :nothing, type: :binary_id)
      add :account_id, references(:accounts, on_delete: :nothing, type: :binary_id)
      timestamps()
    end

    create index(:indications, [:account_indicated_id, :account_id])
  end
end
